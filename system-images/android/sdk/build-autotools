#!/bin/bash

set -e

#
# Determine an appropriate parallelism setting (autotools builds are usually slow enough as it is)
# The jobs formula matches with what ninja seems to think is a good default on my machine, and is fairly conservative anyway.
#
cores="$(nproc --all)"
jobs="$(($cores * 5 / 4))"

NAME="$1"
GIT_URL="$2"
GIT_BRANCH_OR_TAG_REF="${3:-master}"
WORKDIR="$(pwd)/src/$NAME"

if [ -z "$NAME" ]
then
    echo "No name specified (usage: $(basename "$0") <name> <url> <optional tag/branch>)" >&2
    exit 1
elif [ -z "$GIT_URL" ]
then
    echo "No git url specified (usage: $(basename "$0") <name> <url> <optional tag/branch>)" >&2
    exit 1
else
    echo "Will attempt to build '$NAME' ($GIT_BRANCH_OR_TAG_REF) from $GIT_URL"
fi

if [ -e "$WORKDIR" ]
then
    echo "There appears to be something else already at '$WORKDIR'" >&2
    echo "This may mess up your build of '$NAME', proceeding to remove it with prejudice..." >&2
    rm -rf "$WORKDIR"
fi

generate_autotools_script ()
{
    cat << SCRIPT
    ./autogen.sh
    ./configure --prefix=/opt/kdeandroid-deps --host="\${ANDROID_NDK_TOOLCHAIN_PREFIX}"
    make -j$jobs
    make install
SCRIPT
}

git clone --depth 1 --branch "$GIT_BRANCH_OR_TAG_REF" "$GIT_URL" "$WORKDIR"

cd "$WORKDIR"
/opt/helpers/build-standalone "$(generate_autotools_script)"

echo "Build of $NAME succeeded, cleaning up $WORKDIR ..."
rm -rf "$WORKDIR"

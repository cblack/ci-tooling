import sys
from xml.etree import ElementTree as ET

for path in sys.argv[1:]:
    tree = ET.parse(path)
    root = tree.getroot()
    if root.attrib['type'] != 'desktop':
        continue

    for md in tree.findall("provides/binary"):
        print(md.text)
